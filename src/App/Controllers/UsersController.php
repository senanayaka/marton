<?php
namespace App\Controllers;

use App\System\Request;
use App\System\Validate;
use App\System\ErrorHandling;
use App\Views\Template;
use App\Models\GenericModel;


/**
 * Class UsersController
 * @package App\Controllers
 */
class UsersController
{
    /**
     * UsersController constructor.
     * Load data from db
     */
    function __construct()
    {
        $this->filterData = Request::all('filterData');
        $this->GenericModel = new GenericModel();
    }

    /**
     * GET Users list
     *
     */
    function getUsers(){

        //get filter data
        $filterRequest=Request::get();
        //call the model to get data
        $userListData = $this->GenericModel->getData($filterRequest);
        Template::loadView("usersView",$userListData);

    }

    /**
     * Add New User
     * Load add new user form
     */
    function addUser(){

        Template::loadView("addUserView");

    }

    /**
     * User login
     *
     */
    function login()
    {

        if ($_POST) {
            $loginData['email'] = Validate::isEmpty(Request::post('email')) ? $this->setError('email', 'empty') : Request::post('email');
            $loginData['password'] = Validate::isEmpty(Request::post('password')) ? $this->setError('password', 'empty') : Request::post('email');

            if (Validate::getError()) {

                $errorArray=Validate::getError();

            }else{

                $this->checkuserDetails($loginData);

            }
        }

        Template::loadView("userLoginView",$errorArray);


    }

    function setError($fieldName,$condition){

        Validate::setError($fieldName,$condition);
        return $fieldName." ".$condition;

    }

    function checkuserDetails($loginData){

        return  $userData = $this->GenericModel->checkUser($loginData);

    }



}


