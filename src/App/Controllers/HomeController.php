<?php
namespace App\Controllers;

//Trait for handling request data
use App\System\Request;

//Validate data
use App\System\Validate;

//Error handling
use App\System\ErrorHandling;

//Template for display data
use App\Views\Template;

//Common model
use App\Models\GenericModel;


/**
 * Class HomeController
 * @package App\Controllers
 */
class HomeController
{
    /**
     *  Constructor.
     */
    function __construct()
    {

    }

    /**
     * GET Home view
     *
     */
    function index(){

        Template::loadView("homeView");

    }





}


