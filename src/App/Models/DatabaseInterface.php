<?php
namespace App\Models;

/*
 * Interface for front controller
 *
 */

interface DatabaseInterface
{
    //set requestHandler function
    public function connection();

}