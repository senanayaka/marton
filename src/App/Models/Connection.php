<?php
namespace App\Models;
use App\Config\Config;
/**
 * Class Database
 * @package App\Models
 */
class Connection implements DatabaseInterface
{
    private $connection;
    /**
     * GenericModel constructor.
     */
    public function __construct()
    {
        // TODO: Implement __construct() method.'

    }

    public function connection()
    {
        $dbDetails =  Config::DATABASE;
        // Create connection
        $link = mysqli_connect($dbDetails['host'], $dbDetails['user'], $dbDetails['password'],$dbDetails['database'],$dbDetails['port']);
        if (!$link) {
            die('Could not connect: ' . mysqli_error());
        }else{
            return $link;
        }

    }



}