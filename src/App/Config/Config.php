<?php
namespace App\Config;

/**
 * Class Config
 * @package App\Config
 */
class Config
{

    const BASE_URL = '/marton/';

    //Response status constant array
    const STATUS = [
        200 => 'OK',
        404 => 'Not Found',
        405 => 'Method Not Allowed'
    ];

    //Application route constant defined
    const ROUTES = [
        'users/list' =>
            [
                'controller' => 'Users',
                'method' => 'GET',
                'PATH' => 'getUsers'
            ],
        'user/add' =>
            [
                'controller' => 'Users',
                'method' => 'GET',
                'PATH' => 'addUser'
            ],
        'user/save' =>
            [
                'controller' => 'Users',
                'method' => 'GET',
                'PATH' => 'saveUser'
            ],
        'home' =>
            [
                'controller' => 'Home',
                'method' => 'GET',
                'PATH' => 'index'
            ],
        'user/login' =>
            [
                'controller' => 'Users',
                'method' => 'ANY',
                'PATH' => 'login'
            ],

    ];

    const DATABASE = [
            'host' => '127.0.0.1',
            'port' => '8889',
            'user' => 'root',
            'password' => 'root',
            'database' => 'marton_db'
            ];


}