<?php
namespace App\Views;


/**
 * Class APIController
 * @package App\Controllers
 */
trait Template
{
    /**
     * Request constructor.
     */
    public function __construct()
    {

    }

    /**
     * @param mixed $viewFile
     * @param mixed $DataArray
     * @return mixed
     */
    public function loadView($viewFile = false,$DataArray = false)
    {
        //Define file path
       $file="./src/App/Views/".$viewFile.".html";

        //Check file existent
        if (!file_exists($file)) {
            return "Error loading template file ($file).";
        }

        //Get file content
        $output = file_get_contents($file);

        if($DataArray != false){
            //Assign parameter
            $values=$DataArray;
            //loop the array and assign to variable and replace text
            foreach ($values as $key => $value) {
                 $key=array_keys($value)[0];
                 $tagToReplace =   "[@$key]";
                 $output = str_replace($tagToReplace,"Field Cannot ".$value[$key], $output);
            }
        }


       echo $output;

    }







}


