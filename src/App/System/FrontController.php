<?php

namespace App\System;

use App\Config\Config;

/**
 * Class AppCore
 * Front controller core functions defined
 */
class FrontController extends Config implements FrontInterface
{
        protected $status;

        protected $routes;

        protected $baseUrl;


    /**
     * FrontController constructor.
     */
    public function __construct()
    {
        $this->status = Config::STATUS;

        $this->routes = Config::ROUTES;

        $this->baseUrl = Config::BASE_URL;

        $this->requestHandler();

    }

    /**
     * Function for handling the request
     * @param path ,routes
     * @return mixed
     */
    public function requestHandler()
    {
        //Receive url , method and parameters
        $RequestUrl = explode('/', rtrim($_SERVER[REQUEST_URI], '/'));

        if(isset($RequestUrl[2])){
            $RequestMethod = explode("?", $RequestUrl[2], 2)[0];

        //$RequestParameters = explode("?", $RequestUrl[2], 2)[1];

        //Define path
        $path = $RequestUrl[1] . "/" . $RequestMethod;

        }else{

            $path = $RequestUrl[1];

        }

        //Check route is exists
        if ($this->checkRoutes($path, $this->routes)) {

            $this->response(404);

        } elseif ($this->checkMethod($path)) {

            $this->response(405);

        } else {

            $this->setController($path,$RequestUrl);

        }

    }


    /**
     * Check route is exists
     * @param path ,routes
     * @return boolean
     */
    protected function checkRoutes($path, $routes)
    {
        if (!array_key_exists($path, $routes)) {

            return true;

        } else {

            return false;

        }
    }


    /**
     * Check method type
     * @param GET /POST/PUT/DELETE
     * @return boolean
     */
    protected function checkMethod($path)
    {

        if($this->routes[$path]['method'] == "ANY"){

            return false;

        }else if ($this->routes[$path]['method'] != $_SERVER['REQUEST_METHOD']) {

            return true;

        } else {

            return false;

        }

    }



    /**
     * @param int $status
     * @return mixed
     */
    public function response($status = 200)
    {
        header("HTTP/1.1 " . $this->status[$status] . " ");

        echo json_encode($this->status[$status]);

        exit();

    }


    public function setController($path,$RequestUrl){

        //Set the controller from request url
       $RequestObject = "App\\Controllers\\" . $this->routes[$path]['controller'] . "Controller";

        //Create object
        $controller = new $RequestObject();

        //url second parameter represent as  controller - > method
        if (isset($RequestUrl[1])) {

            //Check is method exisist Accordidng to request and predefined array in config
            if (!method_exists($controller, $this->routes[$path][PATH])) {
                $this->response(404);
            } else {
                return $controller->{$this->routes[$path][PATH]}();

            }

        }


    }



}