<?php
namespace App\System;

/*
 * Interface for front controller
 *
 */

interface FrontInterface
{
    //set requestHandler function
    public function requestHandler();

}