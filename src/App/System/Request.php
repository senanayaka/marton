<?php
namespace App\System;

/**
 * Trait Request
 * @package App\System
 * handle the get,post,put,delete request
 */
trait Request
{
    /**
     * Request constructor.
     */
    public function __construct()
    {

    }

    /**
     * @param mixed $reqParam
     * @return mixed
     */
    public static function all($reqParam = false)
    {
        return $reqParam ? $_REQUEST[$reqParam] : $_REQUEST;
    }

    /**
     * @param mixed $reqParam
     * @return mixed
     */
    public static function post($reqParam = false)
    {
        return $reqParam ? $_POST[$reqParam] : $_POST;
    }

    /**
     * @param mixed $reqParam
     * @return mixed
     */
    public static function get($reqParam = false)
    {
        return $reqParam ? $_GET[$reqParam] : $_GET;
    }


}


