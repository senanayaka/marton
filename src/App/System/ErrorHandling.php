<?php
namespace App\System;

/**
 * Trait ErrorHandling
 * @package App\System
 */
trait ErrorHandling
{
    /**
     * Request constructor.
     */
    public function __construct()
    {

    }

    /**
     * @param mixed $reqParam
     * @return mixed
     */
    public static function showError($reqParam = false)
    {
        return $reqParam ? $_GET[$reqParam] : $_GET;

    }



}


