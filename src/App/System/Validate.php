<?php
namespace App\System;

/**
 * Class APIController
 * @package App\Controllers
 */
trait Validate
{
    public static $errorArray= array();

    /**
     * Request constructor.
     */
    public function __construct()
    {

    }

    /**
     * @param mixed $reqParam
     * @return mixed
     */
    public static function isEmpty($reqParam = false)
    {
        return (empty($reqParam)) ?  true : false;
    }

    /**
     * @param mixed $reqParam
     * @return mixed
     */
    public static function isString($reqParam = false)
    {
        return (is_string($reqParam)) ? true : false;
    }

    /**
     * @param mixed $reqParam
     * @return mixed
     */
    public static function isEmail($reqParam = false)
    {

    }

    /**
     * @param mixed $reqParam
     * @return mixed
     */
    public static function isNumeric($reqParam = false)
    {

    }

    public function setError($fieldName,$condition){

        $errorData[$fieldName."_error"]=$condition;

        array_push(self::$errorArray,$errorData);

    }

    public function getError(){

       return self::$errorArray;

    }

}


